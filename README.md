# Introduction

**tl;dr** We'll develop four supervised learning models for deciding whether a pair of behavioral patterns comes from the same user.
Those models will be: 1) XGBoost, 2) MLP neural network, 3) simple siamese neural network, 4) siamese recurrent
neural network.


We recently held a workshop at the ["PL in ML" conference](http://plinml.mimuw.edu.pl/#agenda) entitled
"User identification based on keystroke dynamics".
The workshop introduced the problem of verifying a user with the help of behavioral data collected during typing in a password
on a keyboard.
Materials from the workshop are available [here](https://github.com/nethone/keystroke-dynamics) but
you can [run the code right away](https://mybinder.org/v2/gh/nethone/keystroke-dynamics/master?filepath=workshop.ipynb)
by the grace of this new, hot service called [MyBinder.](https://mybinder.org) The idea is that you go through the
notebook as you read this blog post, so it pays off to click "Cell -> Run All" pretty much now.


This blog post serves as a high-level intro to the problem, an overview of the models, and a commentary
on what more can be done to improve the models.
But more importantly: the **purpose** of the workshop (and this blog post, as a consequence) is to give *you*, the
Reader, a piece of working code that you can play with, modify, enhance, and run as easily as possible.


# The set-up

A seamless user verification system would significantly improve the security of our e-accounts, without sacrificing any
of their conveniences.
The idea is to reinforce the standard user verification system (typically: a password) with a system that verifies
whether the user's behavior matches that of the legitimate owner of an account.


Suppose that you've collected behavioral data of every user during registration.
Now, some time after the registration *someone* logs into the account.
We would like to be able to say whether the behavioral pattern of this "someone" matches the behavioral pattern of the
owner of the account.


We'll focus on one particular type of behavioral data, namely: the so-called **keystroke dynamics**, which are sequences
of timestamps corresponding to push/release events recorded during typing.
We will train four binary classification models, which for a pair of keystroke dynamics will predict whether it
was produced by the same user.
Using such a model, we'll check if a new behavioral pattern matches those collected during registration, and based on that
we'll estimate the probability that we are indeed dealing with the account owner.


## Keystroke dynamics

In a general sense, keystroke dynamics relates to the *detailed* timing information describing when each *particular* key
was pressed and released as a person is typing.
But here we'll be working with a less detailed sequence of timing information, namely: we'll only have access to the timing
information, *without* knowing exactly which key was being pressed/released.


## The data set

We'll be using the [Bei Hang data set](http://mpl.buaa.edu.cn/detail1.htm), but you don't need to download it, it's
already included in the workshop repo, in the `BeiHang` directory.
There are two sub-sets of the data: `DatasetA` and `DatasetB` -- the first one was collected in a cyber cafe environment,
and the latter in laboratory conditions.
We'll be working with `DatasetA` since it's more "realistic".


The data set is comprised of timestamp sequences of the form: <br>
       *P<sub>1</sub>, R<sub>1</sub>, P<sub>2</sub>, R<sub>2</sub>, ..., P<sub>n</sub>, R<sub>n</sub>* <br>
where P<sub>i</sub> is the timestamp of *pressing* the i<sup>th</sup> key, and R<sub>i</sub> is the timestamp of
*releasing* the i<sup>th</sup> key.
For each user, we have 4 or 5 such sequences collected during registration, and about 20 sequences recorded during log-in
attempts.
Among those ~20 sequences some came from the actual owner of the account, while other were typed in by imposters.    


The data are read into a `pandas.DataFrame` using the `produce_whole_DF()` function (code cell **[3]**), so that you can
focus more on the models themselves, rather than the process of loading and cleaning the data.


Example keystroke dynamics collected during registration for two users (`"zhuzhu"` and `"qianhai18"`) are plotted
in code cells **[5]** and **[6]**.
Additionally, there are also plots for the first order difference of timestamps (calculated using the `np.diff` function).
What I mean by *first order difference* is a sequence `out` produced from sequence `seq` in the following manner:
`out[n] = seq[n+1] - seq[n]`, or in a single step: `out = np.diff(seq)`.  


#### Keystroke dynamics features

There are two prominent characteristics of the keystroke dynamics: the *dwell* and *flight* times.


The dwell time is the duration of a key being pressed, while the flight time is the time period between releasing a key
and pressing the next key.
![alt text](img/keystroke.png)

Dwell and flight times are calculated using functions defined in code cell **[7]** and their values (again, for users
`"zhuzhu"` and `"qianhai18"`) are plotted in code cells **[8]** and **[9]**.

Now, these sequences (keystroke dynamics, dwell and flight times) differ in length between users, whereas most supervised
models require feature vectors of unified length.
So, in order to satisfy these requirements, we'll need to somehow aggregate (or: summarize) these sequences of variable
lengths.
We'll take the mean and the standard deviation of the dwell times, of the flight times, but also of the first order difference.
These features are calculated in the `extract_features` function (code cell **[7]**), yielding a single feature vector
of length=6 for each keystroke dynamics sequence.
But notice that within the `extract_features` there is a comment: `# TODO(1): add more features`.
TODOs like this one are scattered throughout this notebook, and the number (1, 2, or 3) stands for difficulty, 1 being
"easy", and 3 being "hard/complex".


#### Validation

It's easy to trick oneself into thinking that the model works fine, when in fact its performance is due to some sort
of data leakage.
Thus, it's important to split the data into train and test sets *according to user names*.
Otherwise, the performance on the test set would be an over-optimistic estimation of how well the model performs for new,
unseen data (in this case: new users).


That said, the Bei Hang data set is not exactly the cleanest, error-free collection.
To make sure the results reported on the test set are not prone to defects in the data, I hand-picked a set of users names,
`test_user_names` (code cell **[12]**), which we'll use to define the test set, and the train set as its complement.
This is not quite legit -- I'm using some arbitrary criteria for choosing the train and test sets, rather than focusing
on making both of them unbiased samples of the underlying distribution.
However, for the purpose of this blog post, let's assume that the keystroke dynamics are indeed *i.i.d.* for different users,
and that there is no confounding factor causing certain users to have some of their sequences damaged.

But going back, we've mentioned in "The set up" section that we'll train binary classification models.
For that, we'll pair up the observations, and label them: `1` if the pair belongs to a single user, and `0` otherwise.
Training and validation will be performed only on pairs of observations collected during registration -- see the
`DF_registration_train` variable in cell **[12]**. (BTW, throughout the code the "`DF_`" prefix indicates that the
variable is a `pandas.DataFrame`.)    


## Models and model training

We will train four models:
1. XGBoost;
2. Multi-layer perceptron (MLP);
3. Simple siamese neural network;
4. Siamese recurrent neural network.


In terms of what the model takes as input, we are dealing with two very different types of models.
The first type (XGBoost & MLP) are models which we'll supply with *absolute differences* of features extracted from
keystroke dynamics sequences:

![alt text](img/feature_difference.png)

and in code calculated simply as: `abs_diff = np.abs(a - b)`.
The labels denote whether the two sequences come from the same user.
In other words: we'll train the model on a feature matrix, whose rows denote how two keystroke dynamics sequences differ.  


The second type of models (the siamese models) are those that take as input full information describing both
observations:

![alt text](img/feature_concat.png)

The "Simple siamese neural network" will take two concatenated feature vectors (each describing a keystroke dynamics
sequence), and that's exactly what's being shown in the image above.
However, the "Siamese recurrent neural network" will take two *almost* raw sequences describing the two keystroke dynamics.
The "almost" is because they're actually going to be the first order differences of sequences of timestamps (recall
the `np.diff` function).


#### Model training and validation

To train and validate the model we'll use the `custom_cross_val_auc_score` function (code cell **[15]**).
What's "custom" about it is that for each fold it splits the data into train and validation sets, according to user
names.
This way, if a given user was used for training, it won't be used for validation.


The first argument of this function, `estimator`, is an object with two methods: `estimator.fit` and `estimator.predict_proba`.
Technically, most classifiers available in `sklearn` implement these methods (but not all have the latter; see for example
`sklearn.svm.LinearSVC`), so it seems that `estimator` might simply be a bare-bone classifier.
However, the `estimator` object is also required to have additional attributes: `estimator.only_feature_diffs` and
`estimator.feature_names`.
The first one is used to determine whether the classifier will take two feature vectors, one per each keystroke dynamics
sequence, or rather their difference.
The latter attribute is used to locate which columns of the input frame (`DF_registration`) are used as features.


But we might need to standardize the data or transform it before passing to the classifier.
To retain the attributes expected of the `estimator` object, we can wrap a list of preprocessing and transformation steps (along
with a classification step at the end), with the `sklearn.pipeline.Pipeline` class.
This is pretty standard practice, and if you haven't encountered it previously -- now's your chance.
Then, we can add the `estimator.only_feature_diffs` and the `estimator.feature_names` attributes.
And that's exactly what we're going to do when training our models in code cells: **[16]**, **[21]**, **[26]**, and
**[29]**.


#### 1. XGBoost

The `xgboost.XGBClassifier` is a robust model in that it gives good results out-of-the-box for a wide range of
problems, without the need of adjusting the hyper-parameters.
We won't be able to go into details of how XGBoost is trained in this blog post, but for perhaps the best introduction
into the algorithm, make sure to check out [this source.](http://xgboost.readthedocs.io/en/latest/model.html)


For our purposes it suffices to know that XGBoost offers a good base-line for our subsequent, neural-net-based models.
If we'll be able to reach XGBoost's default performance without much meddling, we'll consider a model "easy to handle".
We'll see that, while multi-layer perceptrons are easy to handle, siamese architectures require significantly more care.   


As an aside, one drawback of XGBoost is that it's difficult to interpret and even with the aid of tools like
[LIME](https://github.com/marcotcr/lime) or [ELI5](https://github.com/TeamHG-Memex/eli5), understanding how exactly the
model makes its predictions poses a challenge.


The XGBoost model is trained in code cell **[16]**.
We plot the AUC scores calculated by the `custom_cross_val_auc_score` as a histogram.
And although I do admit a histogram of four values looks a bit silly, it becomes justifiable once you use a higher
number of folds (the `num_folds` argument of the `custom_cross_val_auc_score` function).
As a bonus, we can take a peek into which features were important for the construction of the XGBoost model, using the
`xgboost.plot_importance` function.
Sure enough, the means of dwell and flight times were the most relevant features, and it seems that they are quite
informative as the model achieves high values of AUC scores of about 0.90 on validation.


For now, we'll keep the `best_xgb_pipe`, and use it at the very end of this notebook, to see how well it performs on the
test set, along with the rest of the models.
You might play with the arguments of the `xgboost.XGBClassifier`, and get an even better score, but we would advise to
leave that spunk for the siamese recurrent neural network.


#### 2. Multi-Layer Perceptron

The Multi-Layer Perceptron (MLP) is an example of a wide family of models called neural networks.
It's a simple example, but because of that it's worth taking a slightly deeper look into, before moving on to siamese neural networks.


MLP is a stack of subsequent layers of neurons (hence the name).
The picture below depicts a small, three-layered network.
MLP is a type of the so-called *feed-forward* architecture, i.e. each neuron takes as input activations of all neurons from the previous layer, processes it in a certain way (we'll look into that in a moment) and is then used as input by the next layer.


So, what does a neuron "do" exactly?
Let's assume that our input data is a vector **x**.
Each neuron is calculating a dot product of the input vector **x** and this neuron's vector of weights **w** -- that is, multiplying each input coordinate (*x*<sub>i</sub>) by a weight (*w*<sub>i</sub>) and summing it all up to a single number.
Then, a value called *bias* is added to the dot product, and finally, the result is transformed by some non-linear function, *f*.


Since there are multiple neurons in each layer, we can represent the layer as a matrix, with columns corresponding to weight vectors of individual neurons.
The biases are, on the other hand, represented as a vector, **b**.
Those weights and biases are what makes a model provide (hopefully) valuable predictions, and finding them is the main goal of training a neural network model.
Using linear algebra's notation, we can represent our whole model in just a few simple equations seen next to the visualization of a network:

![alt text](img/mlp.png)

Thanks to the non-linearity of the activation functions, our model can estimate almost any mapping from inputs to output.
That is, if the number of layers and neurons in them are sufficient.
The only thing we have to do now is find the optimal weights and biases for each layer (collectively called *parameters*).


As in any machine learning problem, we train the model by minimizing some loss function on the training set.
We know that the loss function depends on predictions from our model, but using the equations mentioned earlier we can also see that predictions depend (more indirectly) on weights and biases.


Because we can define our loss as a function of the parameters, we can also differentiate it with respect to those variables -- and that gives us information about how the loss functions changes as we change the parameter values.
We can then decrease the loss function iteratively, by making tiny updates to the parameters, proportional to the derivatives of the loss function with respect to those parameters.


However, there is much more to training neural networks, and machine learning in general, than just minimizing the loss function on the training set (although this alone can be a challenge).
We would like the model to generalize well to new, unseen data.
This is one of the practical differences between neural nets and the previously mentioned XGBoost model -- they're not as good "out-of-the-box", and typically require more effort to make them generalize well.


Another difference is that we need to standardize the input feature values.
This is done using the `RobustScalerForPandas` (defined in code cell **[20]**), which is a wrapper that makes sure that
the transformed frame is a `pandas.DataFrame`.
This wrapper inherits from the `sklearn.preprocessing.RobustScaler`, and it's interesting to check if there are scaling
strategies leading to better results.


The MLP model is defined in code cell **[21]**; its architecture is specified by parameters passed at initialization,
but the activation functions and the optimization procedure are hard-coded.
Also, note that there's a considerable class imbalance in our data set, so maybe it might be worth playing with the
`class_weight` argument passed to the `model.fit` method?
 

But even without much tweaking of the hyperparameters, the MPL model performs pretty well -- we get AUC scores of about
0.90, which is pretty similar to what we got using XGBoost.


#### 3. Simple siamese neural network

Let's move to code cell **[24]**, where we can see how the input data frame looks like when
`estimator.only_feature_diffs` is `False`.
Note, that the `custom_cross_val_auc_score` function actually calculates this frame internally, and we're not going to
use the `DF_train_all_pairs` variable for anything other than seeing how it looks like.
Each column (besides the `label` column) either ends with an "`_A`", or with a "`_B`".
These suffixes signify to which part of the input a given column belongs to in the siamese model:

![alt text](img/siamese_mlp.png)


**Input A** and **Input B** corresponds to two keystroke dynamics observations.
Then, both inputs are processed by a three-layered neural network, whose activations on the last hidden layer (**Hidden
layer 3**) are then used to calculate their absolute difference (**L1 merge** layer for short), followed by a single
hidden layer (**One more hidden layer**), and finally leading to the **Output** layer which returns a value between `0`
and `1`.
That's how we get our estimate of the probability that the two sequences, described by Input A and Input B, come from
a single user.


There are two important notes here.
First note: the three-layered networks built on the Inputs are actually two "references" to **the same network**.
That is, these two *legs* share the same weights -- if we were to modify any part of the left leg, the right leg would
change as well.
Second note: the order of observations supplied as Inputs does not matter, because the **L1 merge** layer performs a
symmetric operation.
The *symmetry* refers to the arguments, i.e. any *op* such that *op(A, B) = op(B, A)* would also work.
In fact, you can play with this *op*, and use, for example:
```python
def l2_diff(x): return K.pow(x[0] - x[1], 2)
```
which corresponds to *op(x<sub>0</sub>, x<sub>1</sub>) := (x<sub>0</sub> - x<sub>1</sub>)<sup>2</sup>*,
but be aware that the "*-*" and "*(   )<sup>2</sup>*" operations are performed in a vectorized manner.


Note that this time, we set the `only_feature_diffs` attribute to `False` since we want the
`custom_cross_val_auc_score` to use a data frame with actual feature vectors, and not an absolute difference between
them.


On validation, the simple siamese model seems to perform OK (again, the AUC is ~0.90).
But keep in mind, that we still need to measure performance of the model on the test set.


#### 4. Siamese recurrent neural network

We now move to code cell **[30]**, where we train another siamese model:

![alt text](img/siamese_gru.png)

but with a twist: instead of a three-layered siamese legs, we use recurrent units, operating on sequences rather
than on handcrafted features.
We would like these recurrent units to *learn features* as valuable, or better, than aggregated statistics of dwell and
flight times.


Obviously, we might get better results if we merged the activations from the GRU with an MLP working on our standard
features (remember? the mean and std of the dwell times, etc.).
Or even if we used dwell and flight times sequences themselves.
But that's not what we had in mind -- we would really like the model to learn the features directly from the sequence,
without any expert knowledge.
It so happens that we knew that dwell and flight times are indeed valuable characteristics, but what about other
types of behavioral data, e.g. mouse movements?
We know that's utopian thinking, but we believe that some day we'll be able to replace the skill of coming
up with great features (typically pertinent to one, narrow domain) with the skill of coming up with general-purpose neural
network architectures, capable of extracting even better features from (almost) raw data.

![alt text](img/i_have_a_dream.jpg)


The AUC scores on validation are above ~0.80, but slightly worse than previous models.
There are two TODOs in code cell **[30]** with suggesting for improving the model.
(I quick thought: "Two TODOs" would be a great name for a band! No? Never mind...)


But that's not all; you can:
* experiment with a different recurrent units (`GRU` is hardcoded in the
`RNNSiameseModel._create_base_network` method defined in cell **[29]**);
* change the number of neurons of the recurrent unit (again, it's hardcoded);
* increase the number of epochs;
* add regularization;
* surprise us with something else entirely.


# Results on the test set

OK, we have our `best_*_pipe` models trained and ready, we can test them on the non-registration data, for user
specified in the `test_user_names` list at the beginning of the notebook.


Scroll to code cell **[31]**, where you'll find the `test_user_verification` function, which takes care of testing, and
produces a list of AUC scores for the verification process.
You can go through the function to fully understand its mechanics, but what's important is that:
- this time, we use the registration sequences as "templates";
- for a new (non-registration) sequence, we pair it up with every registration sequence for that user;
- for each pair we take the model and estimate the probability that it comes from the same user;
- finally, we take the mean of the predictions, and take that as our final prediction of the new sequence belonging to
the legitimate user.


Results for our `best_*_pipe`-s can be produced by running cells from **[33]** to **[36]**, and... *voila*:
1. XGBoost has the highest mean AUC (0.9880);
2. The MLP model is second (0.9856);
3. Simple siamese model is third (0.9272), but one user had "definitely wrong" predictions;
4. And our most complicated model is fourth (0.8907).


At first glance, it seems that simplicity pays off.
But notice that XGBoost is far from simple, and it wouldn't perform as good if we didn't know that dwell and flight
times are valuable for verifying users.
If we used only the aggregated statistics of the first order difference, XGBoost's mean AUC score would drop to ~0.91,
and that's not that far from the performance of the siamese recurrent neural network.


Now, on the other hand, we can give you a hint that a single change to the `RNNSiameseModel` can pull the mean AUC score
on test to ~0.94 (without adding dwell/flight times).
Can you guess what is it?


And more importantly: can you achieve an even higher score with the `RNNSiameseModel`?


# Parting words

If you've managed to go through the whole blog post -- congratulations!
It was a long one, but hopefully it was worth it.


If you have any questions regarding this blog post, add a comment.
But if it's more code-related, we would appreciate it if you opened an Issue at the workshop repo.


And of course, if you manage to enhance the siamese architectures, you can fork the repo, and provide a link to your
version in the comments for everyone to see!
